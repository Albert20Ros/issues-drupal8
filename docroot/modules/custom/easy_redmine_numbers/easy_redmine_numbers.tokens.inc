<?php
use Drupal\Core\Render\BubbleableMetadata;
/**
 * @file
 * File for custom token hooks.
 */

/**
 * Implements hook_token_info().
 */
function easy_redmine_numbers_token_info() {
  $redmine['projects-Total-Count'] = [
    'name' => t('easy redmine projects Total Count'),
    'description' => t('Get projects Total Count from easy redmine'),
  ];

  $redmine['issues-Total-Count'] = [
    'name' => t('easy redmine issues Total Count'),
    'description' => t('Get issues Total Count from easy redmine'),
  ];

  $redmine['sum-Time-Entries'] = [
    'name' => t('easy redmine sum Time Entries'),
    'description' => t('Get sum Time Entries from easy redmine'),
  ];

  return [
    'tokens' => ['block_content' => $redmine],
  ];
}

/**
 * Implements hook_tokens().
 */

function easy_redmine_numbers_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  $config = \Drupal::service('config.factory')->getEditable('easy_redmine_numbers.settings');
  if ($type == 'block_content' && isset($config)) {

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'projects-Total-Count':
          $replacements[$original] = $config->get('projectsTotalCount');
          break;

        case 'issues-Total-Count':
          $replacements[$original] = $config->get('issuesTotalCount');
          break;

        case 'sum-Time-Entries':
          $replacements[$original] = $config->get('sumTimeEntries');
          break;
      }
    }
  }

  return $replacements;
}
