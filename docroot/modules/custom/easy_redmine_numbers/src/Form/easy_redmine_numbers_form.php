<?php

/**
 * @file
 * Contains \Drupal\easy_redmine_numbers\Form.
 */

namespace Drupal\easy_redmine_numbers\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the easy_redmine_numbers add and edit forms.
 */
class easy_redmine_numbers_form extends EntityForm {

  /**
   * Constructs an ExampleForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): easy_redmine_numbers_form {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'easy_redmine_numbers_form';
  }

  /**
   * {@inheritdoc}
   *
   * Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $easy_redmine_numbers_path = \Drupal::service('extension.list.module')
      ->getPath('easy_redmine_numbers');
    $easy_redmine_numbers = $this->entity;
    $easy_redmine_integration = file_get_contents("{$easy_redmine_numbers_path}/easy_redmine_integration.json");
    $json_integration_obj = json_decode($easy_redmine_integration, TRUE);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $easy_redmine_numbers->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $easy_redmine_numbers->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$easy_redmine_numbers->isNew(),
    ];
    $form['projectsTotalCount'] = [
      '#type' => 'textfield',
      '#title' => 'projectsTotalCount',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $json_integration_obj['projectsTotalCount'],
      '#attributes' => ['readonly' => 'readonly'],
    ];
    $form['issuesTotalCount'] = [
      '#type' => 'textfield',
      '#title' => 'issuesTotalCount',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $json_integration_obj['issuesTotalCount'],
      '#attributes' => ['readonly' => 'readonly'],
    ];
    $form['sumTimeEntries'] = [
      '#type' => 'textfield',
      '#title' => 'sumTimeEntries',
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $json_integration_obj['sumTimeEntries'],
      '#attributes' => ['readonly' => 'readonly'],
    ];
    $form['#attached']['library'][] = 'easy_redmine_numbers/easy_redmine_numbers_theme';
    return $form;
  }

  /**
   * {@inheritdoc}
   */

  public function save(array $form, FormStateInterface $form_state) {
    $easy_redmine_numbers = $this->entity;
    $status = $easy_redmine_numbers->save();

    if ($status === SAVED_NEW) {
      $this->messenger()
        ->addMessage($this->t('The %label easy_redmine_numbers created.', [
          '%label' => $easy_redmine_numbers->label(),
        ]));
    }
    else {
      $this->messenger()
        ->addMessage($this->t('The %label easy_redmine_numbers updated.', [
          '%label' => $easy_redmine_numbers->label(),
        ]));
    }

    $form_state->setRedirect('entity.easy_redmine_numbers.collection');
  }

  public function exist($id): bool {
    $numbers_entity = $this->entityTypeManager->getStorage('easy_redmine_numbers')
      ->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $numbers_entity;
  }

}
