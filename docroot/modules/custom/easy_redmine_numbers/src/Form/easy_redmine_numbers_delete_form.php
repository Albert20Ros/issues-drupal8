<?php

namespace Drupal\easy_redmine_numbers\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to delete an easy_redmine_numbers Entity.
 */
class easy_redmine_numbers_delete_form extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion():string {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('entity.easy_redmine_numbers.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText():string {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()
      ->addMessage($this->t('Entity %label has been deleted.', ['%label' => $this->entity->label()]));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
