<?php

namespace Drupal\easy_redmine_numbers\Controller;

/**
 * Provides a listing of easy_redmine_numbers.
 */

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

class easy_redmine_numbers_list_builder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['label'] = $this->t('Numbers_Entity');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $numbers_entity): array {
    $row['label'] = $numbers_entity->label();
    $row['id'] = $numbers_entity->id();


    return $row + parent::buildRow($numbers_entity);
  }

}
