<?php

namespace Drupal\easy_redmine_numbers\Plugin\Block;

/**
 * @file
 * Contains \Drupal\easy_redmine_numbers\Plugin\Block.
 */

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'easy_redmine_numbers' block.
 *
 * @Block(
 *   id = "easy_redmine_numbers_block",
 *   admin_label = @Translation("easy_redmine_numbers_block"),
 *   category = @Translation("Custom Blocks")
 * )
 */
class easy_redmine_numbers_block extends BlockBase {

  /**
   * {@inheritdoc}
   */

  public function build() {
    $entity = \Drupal::entityTypeManager()
      ->getStorage('easy_redmine_numbers')
      ->load('easy_redmine_numbers_entity');

    if (isset($entity)) {
      $entityBlockBuilder = \Drupal::service('entity.form_builder')
        ->getForm($entity, 'edit');

      return $entityBlockBuilder;
    }
    else {
      return [
        '#type' => 'markup',
        '#markup' => 'entity is not defined! Please create an entity with label (id) easy_redmine_numbers_entity. Delete this block after reading',
      ];
    }
  }

}
