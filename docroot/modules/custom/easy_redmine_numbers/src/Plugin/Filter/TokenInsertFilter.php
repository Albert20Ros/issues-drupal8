<?php
/**
 * @file
 * Contains Drupal\easy_redmine_numbers\Plugin\Filter\TokenInsertFilter.
 */
namespace Drupal\easy_redmine_numbers\Plugin\Filter;
use Drupal\filter\Plugin\FilterBase;
use Drupal\filter\FilterProcessResult;
/**
 * @Filter(
 *   id = "token_insert_filter",
 *   title = @Translation("Replace tokens"),
 *   description = @Translation("Replaces tokens in text."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class TokenInsertFilter extends FilterBase {
  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $text = htmlspecialchars_decode(\Drupal::token()->replace($text));
    $result->setProcessedText($text);
    return $result;
  }
  /**
   *
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Replaces tokens inside text with their values.');
  }
}
