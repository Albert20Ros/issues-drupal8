<?php

namespace Drupal\easy_redmine_numbers\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the easy_redmine_numbers entity.
 *
 * @ConfigEntityType(
 *   id = "easy_redmine_numbers",
 *   label = @Translation("Numbers_Entity"),
 *   handlers = {
 *     "list_builder" =
 *   "Drupal\easy_redmine_numbers\Controller\easy_redmine_numbers_list_builder",
 *     "form" = {
 *       "add" = "Drupal\easy_redmine_numbers\Form\easy_redmine_numbers_form",
 *       "edit" = "Drupal\easy_redmine_numbers\Form\easy_redmine_numbers_form",
 *       "delete" =
 *   "Drupal\easy_redmine_numbers\Form\easy_redmine_numbers_delete_form",
 *     }
 *   },
 *   config_prefix = "integration_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   links = {
 *     "edit-form" =
 *   "/admin/config/development/easy_redmine_numbers/{easy_redmine_numbers}",
 *     "delete-form" =
 *   "/admin/config/development/easy_redmine_numbers/{easy_redmine_numbers}/delete",
 *   }
 * )
 */
class easy_redmine_numbers extends ConfigEntityBase {

  /**
   * The numbers_entity ID.
   *
   * @var string
   */
  public $id;

  /**
   * The numbers_entity label.
   *
   * @var string
   */
  public $label;

}
